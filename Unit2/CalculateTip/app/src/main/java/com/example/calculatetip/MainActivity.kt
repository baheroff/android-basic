package com.example.calculatetip

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.calculatetip.databinding.ActivityMainBinding
import java.text.NumberFormat
import kotlin.math.ceil

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.calculateButton.setOnClickListener {
            calculateTip()
        }
    }

    private fun calculateTip() {

        val cost = binding.costOfService.text.toString().toDoubleOrNull()

        if (cost == null) {
            binding.tipResult.text = ""
            return
        }

        val percent = when (binding.tipOptions.checkedRadioButtonId) {
            R.id.option_twenty_percent -> 0.2
            R.id.option_eighteen_percent -> 0.18
            else -> 0.15
        }

        var tip = cost * percent

        if (binding.roundUpSwitch.isChecked) {
            tip = ceil(tip)
        }

        val formatted = NumberFormat.getCurrencyInstance().format(tip)

        binding.tipResult.text = getString(R.string.tip_amount, formatted)

    }
}